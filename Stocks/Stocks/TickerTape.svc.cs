﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TickerTape
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TickerTape" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TickerTape.svc or TickerTape.svc.cs at the Solution Explorer and start debugging.
    public class TickerTape : Stocks
    {

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }

            return composite;
        }

        public QuoteInfo GetQuote(string tickerSymbol)
        {
            return new QuoteInfo();
        }
    }
}
