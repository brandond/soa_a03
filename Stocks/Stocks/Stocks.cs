﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace TickerTape
{

    public class QuoteInfo
    {

    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]


    public interface Stocks
    {
        [OperationContract]
        QuoteInfo GetQuote(string tickerSymbol);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        QuoteInfo quoteInfo = new QuoteInfo();

        [DataMember]
        public QuoteInfo QuoteInfo
        {
            get { return quoteInfo; }
            set { quoteInfo = value; }
        }
    }
}
